        /*
 * Copyright (C) 2020 urnusdev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uide;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uViews.View;
import uViews.core;
/**
 *
 * @author urnusdev
 */
@WebServlet(name = "init", urlPatterns = {"/urnusdev"})
public class init extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       request.setAttribute("titulo", "UDev 0.5");
/*       a initsnip= new a();
       request.setAttribute("init_snip", initsnip.init()); */

         View view = new View(request, response);
            String uPath = request.getServletPath();
       /* ServletContext sc = request.getServletContext();
        String scn = sc.getServletContextName();
        String gcp = sc.getContextPath();
        StringBuffer requestURL = request.getRequestURL();
        String qs = request.getQueryString();
        ServletRegistration sr = sc.getServletRegistration(scn);
        String uri = request.getRequestURI();
        uri.replace("UDevel/", "");*/
       
        // request.getRequestDispatcher("ide-components/main.jsp").include(request, response);
              
             if (uPath.equals("/index.html")||uPath.equals("")){
             view.show("index_.jsp");
             }else{
            String qps = "";
                 core viewcore=new core(request,response,qps);
                //   view.show("UDevel/ide-components/main.jsp");
             }
         
    }
public void extern_start(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

  processRequest(request, response);

}
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Principal Servlet";
    }// </editor-fold>

}
