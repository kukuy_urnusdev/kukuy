/*
 * Copyright (C) 2020 urnusdev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uide.listeners;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import uViews.GlobalSNIP;
/**
 * Web application lifecycle listener.
 *
 * @author urnusdev
 */
public class context implements ServletContextListener, ServletContextAttributeListener {

    private Object a;
    private ServletContextEvent sce;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
  //     sce.getServletContext().setAttribute("placeholder_project_name", "Project Name");
   //     ClassLoader S = sce.getServletContext().getClassLoader();
   //     Class<? extends ServletContext> X = sce.getServletContext().getClass();
   //     String f = sce.getServletContext().getRealPath("/");
  
   //uccore controllers = new uccore(request,response,request.getServletContext());
   //this.getRouter()!=null;
   
    GlobalSNIP gs=new GlobalSNIP();
    gs.function(sce.getServletContext());

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    @Override
    public void attributeAdded(ServletContextAttributeEvent scae) {
        this.a=scae.getValue();
        try{
       // String snip=scae.getValue().toString().replace("/*"+scae.getName()+"*/", "");
      //  scae.getServletContext().setAttribute(scae.getName(), snip);
        }catch(Exception e){}
      
    }

    @Override
    public void attributeRemoved(ServletContextAttributeEvent scae) {
    }

    @Override
    public void attributeReplaced(ServletContextAttributeEvent scae) {
    }
}
