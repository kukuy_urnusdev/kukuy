/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uControllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import file.json;

/**
 *
 * @author urnusdev
 */
public  class uccore {

    private final String router;
    private final json JSON;
    private final HttpServletRequest request;
  public uccore(HttpServletRequest request, HttpServletResponse response){
     this.request=request;
         JSON=new json();
         router=request.getServletContext().getInitParameter("controller_router");
         this.load();
  }
  

    public String getRouter() {
        return router;
    }
    
    private void load()
    {
        String file = request.getServletContext().getRealPath(getRouter());
    this.JSON.read(file);
    }

    public json getJSON() {
        return JSON;
    }

}
