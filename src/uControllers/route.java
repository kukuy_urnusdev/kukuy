/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uControllers;

import core.defaultFilter;
import file.json;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author urnusdev
 */
public class route extends HttpServlet {

    Constructor constructor;
    public ServletRequest request;
    public ServletResponse response;
    public HttpServletRequest HttpRequest;
    private uccore ucore;

    public void _start() throws ClassNotFoundException, NoSuchMethodException, InstantiationException {

        ucore = new uccore(this.HttpRequest, (HttpServletResponse) this.response);

        String Router = ucore.getRouter();
        json controladores = ucore.getJSON();

        Iterator<Map.Entry<String, Object>> it = controladores.jsonObj.toMap().entrySet().iterator();

        String c = ((HttpServletRequest) request).getServletPath().substring(1);// replace("/", "");
        JSONArray controller_info = null;
        try {
            controller_info = (JSONArray) controladores.jsonObj.get(c);
        } catch (JSONException e) {
            //       System.out.println(" not Found in urouter.json , continue execution || si no existe en este array continuamos con la ejecucion");
            return;
        }

        JSONObject ci = controller_info.getJSONObject(0);

        JSONObject n_controller = (JSONObject) ci.get("controller");

        String class_ = n_controller.getString("class");
        String pkg_ = n_controller.getString("pkg");
        String method_ = n_controller.getString("method");

        String Controller = pkg_ + "." + class_;
        ClassLoader classLoader = defaultFilter.class.getClassLoader();

        Class aClass = classLoader.loadClass(Controller);
        try {

            System.out.println("aClass.getName() Urnus = " + aClass.getName());

            constructor = aClass.getConstructor(
                    new Class[]{
                        ServletRequest.class,
                        ServletResponse.class,
                        HttpServletRequest.class
                    });
            Object ControllerObj = constructor.newInstance(this.request, this.response, this.HttpRequest);

            Method[] methods = aClass.getMethods();
            int index = 0;
            for (int i = 0; i < methods.length; i++) {
                System.out.println(methods[i].getName());
                if (methods[i].getName().equals(method_)) {
                    index = i;
                    break;
                }
            }
            methods[index].invoke(ControllerObj);
            System.out.println("End execution controller ");
            response.getWriter().flush();
            //    response.getOutputStream().close();

        } catch (JSONException e) {
            System.out.println(e.getMessage());
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException ex) {
            Logger.getLogger(route.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ServletRequest getRequest() {
        return request;
    }

    public void setRequest(ServletRequest request) {
        this.request = request;
    }

    public ServletResponse getResponse() {
        return response;
    }

    public void setResponse(ServletResponse response) {
        this.response = response;
    }

    public void setHttpRequest(HttpServletRequest HttpRequest) {
        this.HttpRequest = HttpRequest;
    }

    public HttpServletRequest getHttpRequest() {
        return HttpRequest;
    }

}
