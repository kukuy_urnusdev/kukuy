/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectsBuidler;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uViews.View;

/**
 *
 * @author urnusdev
 */
public class WorkSpace {

    private final Enumeration<String> parameterNames;
    private final Map<String, String> params;
    private final String real_path;
    private final String servlet_name;
    private final String context_path;
    private final HttpServletRequest request;
    private final HttpServletResponse response;

    public WorkSpace(HttpServletRequest request, HttpServletResponse response, CreateProject cp) {
        this.request = request;
        this.response = response;
        this.servlet_name = cp.getServletName();
        this.context_path = cp.getServletContext().getContextPath();

        this.real_path = cp.getServletContext().getRealPath(File.separator);
        this.params = new HashMap<>();
        this.parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {
            String paramName = this.parameterNames.nextElement();
            String[] paramValues = request.getParameterValues(paramName);
            for (String paramValue : paramValues) {
                this.params.put(paramName, paramValue);
            }
        }
      //  this.CreateDirsWorkspace();
    }

    public void CreateDirsWorkspace() {

        File newFolder = 
        new File(
                "/opt/user_workspaces/" + 
                this.context_path + "/" + 
                this.get_param("uproject-name")
        );

        boolean created = newFolder.mkdirs();

        if (created) {
            System.out.println("Workspace was created !");
        } else {
            System.out.println("Unable to create Workspace");
        }
    }

    public void DialogA() throws IOException, ServletException {
        String url = "ide-dialogs/newproject.jsp";
        // request.getRequestDispatcher(url).include(request, response);
        View view = new View(this.request,this.response);
      //  view.setReal_path(real_path);
     //   view.load(url);
      //  view.show_content();
      view.show(url);
        //      ArrayList<String> view=this.getValues(url);

    }

    public String get_param(String paramname) {

        return params.get(paramname);
    }

    public String getReal_path() {
        return real_path;
    }

    public String getServlet_name() {
        return servlet_name;
    }

    public Map<String, String> getParams() {
        return params;
    }

}
