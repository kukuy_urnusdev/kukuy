/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uViews;

import java.util.ArrayList;
import javax.servlet.ServletContext;
import org.json.JSONArray;
import org.json.JSONObject;
import file.read;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author urnusdev
 */
public class GlobalSNIP {

    private String path;
    private read file;
    private ServletContext sc;
    private JSONArray uviews;
    private String c;
    private ArrayList<String> codesnip;
    private String LastErrorGlobalSNIP;
    private Map<String, String> map;
    private int errors;
    private ArrayList<Object> ErrorsGlobalSNIP;

    public void function() {

    }

    public void function(ServletContext servletContext) {

        file = new read();
        ErrorsGlobalSNIP = new ArrayList<>();
        sc = servletContext;

        this._get_list();

        Iterator<String> ite = this.map.keySet().iterator();
        while (ite.hasNext()) {
            String key = ite.next();
            String snip = map.get(key).replace("/*" + key + "*/", "");
            sc.setAttribute(key, snip);
        }
    }

    public int getErrors() {
        return errors;
    }

    private void _get_list() {

        path = sc.getRealPath("/WEB-INF/uview.list.json");

        file.textfile(path);
        String jsonString = file.getTxt();

        JSONObject obj = new JSONObject(jsonString);
        errors = 0;
        try {
            map = new HashMap<String, String>();
            // JSONObject uviews = obj.getJSONObject("files");
            uviews = obj.getJSONArray("files");
            int sizec = uviews.length()-1;
            for (int ite = 0; ite <= sizec; ite++) {
                c = uviews.getString(ite);
                path = sc.getRealPath("/WEB-INF/snippets/" + c);
                file.textfile(path);
                codesnip = file.getContent();
                String txt = file.getTxt();
                String cs = codesnip.get(0).replace("/*", "").replace("*/", "");
                map.put(cs, txt);
            }
        } catch (Exception f) {
            if (!"".equals(file.getError())) {
                this.ErrorsGlobalSNIP.add(errors, file.getError());
                LastErrorGlobalSNIP = file.getError();

            } else {
                this.ErrorsGlobalSNIP.add(errors, f.getMessage());
                LastErrorGlobalSNIP = f.getMessage();
            }
            errors++;
        }

        //uviews_names = uviews.names();
    }

    public Map<String, String> getMap() {
        return map;
    }

    public ArrayList<Object> getErrorsGlobalSNIP() {
        return ErrorsGlobalSNIP;
    }

    public read getFile() {
        return file;
    }

    public JSONArray getUviews() {
        return uviews;
    }

    public ArrayList<String> getCodesnip() {
        return codesnip;
    }

    public String getLastErrorGlobalSNIP() {
        return LastErrorGlobalSNIP;
    }

    private void uviewList() {
    }
}
