/*
 * Copyright (C) 2020 urnusdev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uViews;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author urnusdev
 */
public class View {

    private String real_path;
    private ArrayList<String> content;
    private final HttpServletResponse response;
    private final HttpServletRequest request;
    private final String uri;
    private ServletResponse ServletResponse;
    private ServletRequest ServletRequest;
     
    
    public View(HttpServletRequest request, HttpServletResponse response){
    this.response=response;
    this.request=request;
    uri = request.getRequestURI();
    }

    
    
     public  void load(String file) {
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(this.real_path+"/"+file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String strLine;
        ArrayList<String> lines = new ArrayList<>();
        try {
            while ((strLine = reader.readLine()) != null) {
                String line = strLine.substring(strLine.lastIndexOf("\n")+1);
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setContent(lines);
       
    }

    protected void setContent(ArrayList<String> content) {
        this.content = content;
    }

    public ArrayList<String> getContent() {
        return content;
    }
    
    public void show_content() throws IOException{
       
         PrintWriter out = this.response.getWriter();  
         this.getContent().forEach(part -> {
             out.println(part);
        });
    }
    
    public void show(String view) throws ServletException, IOException{
    request.getRequestDispatcher(view).include(request, response);
    }

    public void showA(String view) throws ServletException, IOException{
    request.getRequestDispatcher(view).forward(request, response);
    }
    public void setReal_path(String real_path) {
        this.real_path = real_path;
    }


    
    
}
