/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uViews;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uControllers.uccore;
/**
 *
 * @author urnusdev
 */
public class core extends uccore{

    private final HttpServletRequest request;
    private final HttpServletResponse response;
    private final String uri;
    private final String path_info;
    private final String method;
    private final Enumeration<String> header_names;
    private final ServletContext sc;
    private final String context_path;
    private final Map<String, String> params;
    private final Enumeration<String> parameterNames;
    private String[] uripart;
   
    private String CoreControllerError;
    private  boolean exist;

     public core(HttpServletRequest request, HttpServletResponse response, String qps){
          super(request,response);
   System.out.print("UviewaCore Stage\n");
    if( this.getRouter()!=null)
    {
    }else{
    CoreControllerError="Not defined path to route in webxml, Solve: Set initparameter controller_router";
    }

    this.response=response;
    this.request=request;
    sc = request.getServletContext();
     System.out.println("UviewsCore Stage - - - - - -");
       System.out.println("CONTEXT::"+sc.getContextPath());
    context_path=sc.getContextPath();
     String sp = request.getServletPath();
     uri = request.getRequestURI().replace("..", "").replace(context_path, "");
     
     path_info=request.getPathInfo();
method =     request.getMethod();
header_names=request.getHeaderNames();
 System.out.print("UviewsCore Stage + + + + + + + \n");
                        System.out.println("URI::"+this.uri);
String path=request.getServletContext().getRealPath("/WEB-INF/views"+this.uri);
this.params = new HashMap<>();
        this.parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {
            String paramName = this.parameterNames.nextElement();
            String[] paramValues = request.getParameterValues(paramName);
            for (String paramValue : paramValues) {
                this.params.put(paramName, paramValue);
            }
        }
        String u = uri.replace(this.context_path, "");
          
        try {
            if(!"true".equals(this.get_param("loaded"))){
                uripart=this.uri.split("/");
                    String  the_view=uripart[uripart.length-1];
                File f = new File(path);
                    System.out.print("UviewaCore Stage\n");
                        System.out.println("Verificando existencia de:"+path);
          exist=f.exists();
          if(exist){
          System.out.print("UviewaCore Stage\n");
          System.out.print("/WEB-INF/views"+u+"?loaded=true");
             request.getRequestDispatcher("/WEB-INF/views"+u+"?loaded=true").forward(request, response);
         
          }else{
           request.getRequestDispatcher(uri+"?loaded=true").forward(request, response);
            System.out.print("UviewaCore Stage\n");
                  System.out.print(uri+"?loaded=true");
          }
                
                
                
          
            }
        } catch (ServletException | IOException ex) {
              
             
           
        }
    }
    public void init(){
    }
    
    public final String get_param(String paramname) {

        return params.get(paramname);
    }
}
