/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file;

import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author urnusdev
 */
public class json extends read{

    private String jsonString;
    public JSONObject jsonObj;
    
    public json(){
    super();
    }
    public void read(String path){
        textfile(path);
        jsonString = getTxt();
        try{
        jsonObj = new JSONObject(jsonString);
        this.iterate(jsonObj.toMap());
        }catch(Exception e){
        error=e.getMessage();
        }
    }
    
    private void iterate(Map mp){
     Iterator it = mp.entrySet().iterator();
    while (it.hasNext()) {
        Map.Entry pair = (Map.Entry)it.next();
        System.out.println(pair.getKey() + " = " + pair.getValue());
        it.remove(); // avoids a ConcurrentModificationException
    }
    }
}
